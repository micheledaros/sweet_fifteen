package config;

public class Logger {
    public static boolean enabled = false;
    public static void debug (Object s) {
        if (enabled) {
            System.out.println(s);
        }
    }
}
