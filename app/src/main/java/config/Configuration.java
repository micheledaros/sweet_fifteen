/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package config;

import fifteen.gui.BoardPanel;
import fifteen.gui.Images;
import fifteen.model.Board;

/**
 *
 * @author michele
 */
public class Configuration {

    public final static Configuration CONFIGURATION = new Configuration();

    private final Images images = new Images();
    private final Board board = new Board();

    private Configuration() {
    }

    public Images getImages() {
        return images;
    }

    public Board getBoard() {
        return board;
    }
}
