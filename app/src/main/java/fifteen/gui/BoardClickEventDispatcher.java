/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package fifteen.gui;

/**
 *
 * @author michele
 */
public interface BoardClickEventDispatcher {
    void onSwap(int fromIndex, int toIndex);
}
