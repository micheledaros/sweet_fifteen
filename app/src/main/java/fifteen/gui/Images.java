/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package fifteen.gui;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.imageio.ImageIO;

/**
 *
 * @author michele
 */
public class Images {

    public static int WIDTH = 612;
    public static int HEIGHT = 612;

    public String[] getSets() {
        return sets;
    }

    private final String[] sets = new String[] {"AMBRA", "VALENTINO"};

    public Integer getSelectedIndex() {
        return selectedIndex;
    }

    public void setSelectedIndex(Integer selectedIndex) {
        this.selectedIndex = selectedIndex;
    }

    private Integer selectedIndex = 1;

    private final Map<String, Map<Integer, Image>> images =
            Arrays.stream(sets).collect(Collectors.toMap(
                    it -> it,
                    set -> Arrays.stream(ImageResources.values())
                            .collect(Collectors.toMap(
                                    ImageResources::getNumber,
                                    en-> en.loadImage(set)
                            ))
            ));


    Image get(int n) {
        return images.get(sets[selectedIndex]).get(n);
    }

    enum ImageResources {
        ZERO(0, "0.png"),
        ONE(1, "1.png"),
        TWO(2, "2.png"),
        FOUR(4, "4.png"),
        FIVE(5, "5.png"),
        SIX(6, "6.png"),
        SEVEN(7, "7.png"),
        EIGHT(8, "8.png"),
        NINE(9, "9.png"),
        TEN(10, "10.png"),
        ELEVEN(11, "11.png"),
        TWELVE(12, "12.png"),
        THIRTEEN(13, "13.png"),
        FOURTEEN(14, "14.png"),
        FIFTEEN(15, "15.png");

        private final int number;
        private final String path;
        private Image loadImage(String folder) {
            try {
                String fullPath = "/img/" + folder.toLowerCase() + "/" + path;
                InputStream inputStream = Images.class.getResourceAsStream(fullPath);
                Image image = ImageIO.read(inputStream);
                config.Logger.debug("loaded " + fullPath);
                return image;
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }


        public int getNumber() {
            return number;
        }


        ImageResources(int number, String path) {
            this.number = number;
            this.path = path;

        }
    }

}
