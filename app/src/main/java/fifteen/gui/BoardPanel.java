
package fifteen.gui;

import fifteen.model.Board;
import fifteen.model.Move;

import java.awt.*;
import java.awt.event.MouseEvent;

/**
 *
 * @author michele
 */
public class BoardPanel extends javax.swing.JPanel {

    private Board board;
    private final Images images;
    private final BoardClickEventDispatcher clickDispatcher;

    private int squareWidth;
    private int squareHeight;

    private int[] pressedSquare = null;
    private int[] movedCoord = null;
    private int[] movedSquare = null;

    private boolean isInitializing = false;
    
    /**
     * Creates new form BoardPanel
     */
    public BoardPanel(
            Board board, 
            Images images,
            BoardClickEventDispatcher clickDispatcher
    ) {
        this.board = board;
        this.images = images;
        this.clickDispatcher = clickDispatcher;
        initComponents();
    }
    
    public void setIsInitializing(boolean isInitializing) {
        this.isInitializing = isInitializing;
        pressedSquare = null;
        movedCoord = null;
        movedSquare = null;
    }

    private void initComponents() {

        setBackground(new java.awt.Color(255, 255, 153));
        addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                formMouseClicked(evt);
            }

            @Override
            public void mousePressed(MouseEvent e) {
                formMousePressed(e);
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                formMouseReleased(e);

            }

            @Override
            public void mouseExited(MouseEvent e) {
                formMouseExited(e);
            }


        });

        addMouseMotionListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseMoved(MouseEvent e) {
                formMouseMoved(e);
            }

            @Override
            public void mouseDragged(MouseEvent e) {
                formMouseMoved(e);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );
    }

    private void formMouseClicked(java.awt.event.MouseEvent evt) {                                  

    }

    private void formMousePressed(java.awt.event.MouseEvent evt) {
        if (!isInitializing) {
            return;
        }
        int x = evt.getX();
        int y = evt.getY();

        int col = x/ squareWidth;
        int row = y / squareHeight;
        pressedSquare = new int[]{col,row};
        repaint();
        config.Logger.debug("pressed " + col + " " + row);
    }

    private void formMouseReleased(java.awt.event.MouseEvent evt) {
        if (!isInitializing) {
            return;
        }
        if (pressedSquare == null || movedSquare == null) {
            return;
        }
        int x = evt.getX();
        int y = evt.getY();

        if (x > getWidth() || y> getHeight() || x < 0 || y < 0) {
            config.Logger.debug("released out of bounds");

        } else {
            int fromIndex = pressedSquare[0] + pressedSquare[1]*4;
            int toIndex = movedSquare[0] + movedSquare[1]*4;
            clickDispatcher.onSwap(fromIndex, toIndex);
            config.Logger.debug("released " + fromIndex + " " + toIndex);
        }
        repaint();
        pressedSquare = null;
        movedSquare = null;
        movedCoord = null;
    }

    private void formMouseMoved(java.awt.event.MouseEvent evt) {
        if (!isInitializing) {
            return;
        }
        movedCoord = new int[] {evt.getX(), evt.getY()};
        int col = evt.getX()/ squareWidth;
        int row = evt.getY() / squareHeight;
        movedSquare = new int[]{col,row};
        repaint();
        config.Logger.debug("moved");
    }

    private void formMouseExited(MouseEvent e) {
        if (!isInitializing) {
            return;
        }
        movedCoord = null;
        movedSquare = null;
        repaint();
        config.Logger.debug("exited");
    }
    public void setBoard(Board board) {
        this.board = board;
        repaint();
    }

    @Override
    protected void paintComponent(Graphics g) {
        
        Graphics2D g2 = (Graphics2D) g.create();
        g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);

        calculateSquareWidthAndHeight();

        for (int col = 0; col < 4; col++) {
            for (int row = 0; row < 4; row++) {
               Integer number = board.get(col, row);
               if (number != null) {
                   Image image = images.get(number);
                   g2.drawImage(
                           image, 
                           col*squareWidth,
                           row*squareHeight,
                           squareWidth,
                           squareHeight,
                           this
                   );
               } else {
                   g2.clearRect( 
                           col*squareWidth,
                           row*squareHeight,
                           squareWidth,
                           squareHeight);
               }
            }
        }

        var nextMove = board.getNextMove();

        g2.setStroke(new BasicStroke(10));

        if (board.getNextMove() != null) {
            int spaceIndex = board.getSpaceIndex();
            int spaceRow = spaceIndex / 4;
            int spaceCol = spaceIndex % 4;

            int destX = spaceCol*squareWidth + squareWidth/2;
            int destY = spaceRow*squareHeight + squareHeight/2;

            int startX = destX;
            int startY = destY;

            int arrow1X = destX;
            int arrow1Y = destY;

            int arrow2X = destX;
            int arrow2Y = destY;

            int arrowSize = 10;

            if (nextMove == Move.N) {
                startY -= squareHeight;
                arrow1Y -= arrowSize;
                arrow2Y -= arrowSize;

                arrow1X += arrowSize;
                arrow2X -= arrowSize;

            }else if (nextMove == Move.S){
                startY += squareHeight;
                arrow1Y += arrowSize;
                arrow2Y += arrowSize;

                arrow1X += arrowSize;
                arrow2X -= arrowSize;
            }else if (nextMove == Move.W){
                startX -= squareWidth;

                arrow1Y += arrowSize;
                arrow2Y -= arrowSize;

                arrow1X -= arrowSize;
                arrow2X -= arrowSize;
            }else if (nextMove == Move.E){
                startX += squareWidth;

                arrow1Y += arrowSize;
                arrow2Y -= arrowSize;

                arrow1X += arrowSize;
                arrow2X += arrowSize;
            }

            g2.setColor(Color.RED);
            g2.drawLine(startX,startY, destX, destY);
            g2.drawLine(destX, destY,arrow1X,arrow1Y);
            g2.drawLine(destX, destY,arrow2X,arrow2Y);

        }

        if (pressedSquare != null && movedCoord != null && movedCoord != null) {

            Color colorPressed = new Color(255, 0, 0, 127);
            g2.setColor(colorPressed);
            g2.fillRect(
                    pressedSquare[0]*squareWidth,
                    pressedSquare[1]*squareHeight,
                    squareWidth,
                    squareHeight);

            if (pressedSquare[0] != movedSquare[0] || pressedSquare[1] != movedSquare[1]) {
                Color colorReleased = new Color(0, 255, 0, 127);
                g2.setColor(colorReleased);
                g2.fillRect(
                        movedSquare[0] * squareWidth,
                        movedSquare[1] * squareHeight,
                        squareWidth,
                        squareHeight);
            }

            Integer number = board.get(pressedSquare[0], pressedSquare[1]);
            if (number != null) {
                Image image = images.get(number);
                g2.drawImage(
                        image,
                        movedCoord[0],
                        movedCoord[1],
                        squareWidth,
                        squareHeight,
                        this
                );
            }
        }
    }

    private void calculateSquareWidthAndHeight() {
        int width = this.getWidth();
        int height = this.getHeight();
        squareWidth = width/4;
        squareHeight = height / 4;
    }

}
