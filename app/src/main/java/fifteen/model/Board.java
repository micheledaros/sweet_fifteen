/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package fifteen.model;

import java.util.*;
import java.util.stream.Collectors;

import static fifteen.model.Move.*;

/**
 * col - row | column |0 1 2 3 -----|----------- r 0 |0 1 2 o 1 |4 5 6 7 w 2 |8
 * 9 10 11 3 |12 13 14 15
 */
public class Board {

    final static List[] neighbourMoves = new List[]{
        Arrays.asList(S, E), // 0
        Arrays.asList(S, W, E), // 1
        Arrays.asList(S, W, E), // 2
        Arrays.asList(S, W), // 3

        Arrays.asList(N, S, E), // 4
        Arrays.asList(N, S, W, E), // 5
        Arrays.asList(N, S, W, E), // 6
        Arrays.asList(N, S, W), // 7

        Arrays.asList(N, S, E), // 8
        Arrays.asList(N, S, W, E), // 9
        Arrays.asList(N, S, W, E), // 10
        Arrays.asList(N, S, W), // 11

        Arrays.asList(N, E), // 12
        Arrays.asList(N, W, E), // 13
        Arrays.asList(N, W, E), // 14
        Arrays.asList(N, W), // 15
    };

    private final Integer[] board = new Integer[16];
    private final Set<Integer> blocked = new HashSet<>();
    private final Set<Integer> target = new HashSet<>();
    private boolean isLegal = true;
    private int spaceIndex = 3;
    private int moves = 0;
    private int manhattanDistance = 0;
    private int score = 0;

    private Board parent = null;
    private Move lastMove = null;
    private Move nextMove = null;

    public void clear() {
        for (int i = 0; i < 16; i++) {
            board[i] = null;
        }
        moves = 0;
        lastMove = null;
        nextMove = null;
        parent = null;
        blocked.clear();
        target.clear();

    }

    public void resetToSolution() {
        for (int i = 0; i < 16; i++) {
            board[i] = i;
        }
        board[3] = null;
        score();
    }

    public Board copy() {
        Board copy = new Board();
        for (int i = 0; i < 16; i++) {
            copy.board[i] = board[i];
        }
        copy.spaceIndex = spaceIndex;
        copy.lastMove = lastMove;
        copy.moves = moves;
        copy.manhattanDistance = manhattanDistance;
        copy.score = score;
        copy.parent = parent;
        copy.blocked.addAll(blocked);
        copy.target.addAll(target);
        copy.isLegal = isLegal;
        return copy;
    }

    public void swap(int sourceIndex, int destinationIndex) {
        Integer orig = board[sourceIndex];
        board[sourceIndex] = board[destinationIndex];
        board[destinationIndex] = orig;
    }

    public Move getNextMove() {
        return nextMove;
    }

    public Integer get(int col, int row) {
        return board[index(col, row)];
    }

    public void set(int index, Integer value) {
        board[index] = value;
    }

    public int getSpaceIndex() {
        return spaceIndex;
    }

    public int getManhattanDistance() {
        return manhattanDistance;
    }

    public List<Board> neighbours() {
        List<Move> neighboursMoves = neighbourMoves[spaceIndex];
        var neighbours = neighboursMoves.stream().map(it -> {
            var copy = this.copy();
            copy.parent = this;
            copy.move(it);
            return copy;
        }).filter(it -> it.isLegal)
                .collect(Collectors.toList());
        return neighbours;
    }

    public void move(Move move) {
        int row = spaceIndex / 4;
        int col = spaceIndex % 4;
        int targetRow = row;
        int targetCol = col;
        if (move == N) {
            targetRow--;
        } else if (move == S) {
            targetRow++;
        } else if (move == W) {
            targetCol--;
        } else if (move == E) {
            targetCol++;
        }

        int targetIndex = targetRow * 4 + targetCol;
        Integer targetValue = board[targetIndex];
        board[spaceIndex] = targetValue;
        board[targetIndex] = null;
        spaceIndex = targetIndex;
        lastMove = move;
        moves++;
        score();
        isLegal = (!blocked.contains(targetIndex));
    }

    public int score() {
        int localScore = manhattanDistance() * 1 + moves * 100;
        this.score = localScore;
        return localScore;
    }

    public String toString() {
        String all = "";
        for (int row = 0; row < 4; row++) {
            for (int col = 0; col < 4; col++) {
                Integer value = get(col, row);
                if (value != null) {
                    all += value;
                    if (value < 10) {
                        all += " ";
                    }
                } else {
                    all += "  ";
                }
                all += " ";
            }
            all += "\n";
        }
        return all;
    }

    public List<Board> getSolution() {
        List<Board> solution = new ArrayList<>();
        solution.add(this);
        Board localParent = parent;
        nextMove = this.lastMove;
        while (localParent != null) {
            localParent.nextMove = nextMove;
            solution.add(0, localParent);
            nextMove = localParent.lastMove;
            localParent = localParent.parent;
        }
        return solution;
    }

    public Board solve() {
        validate();
        moves = 0;
        lastMove = null;
        nextMove = null;
        blocked.clear();
        target.clear();
        parent = null;

        for (int i = 0; i < 16; i++) {
            Integer value = board[i];
            if (value == null) {
                spaceIndex = i;
            }
        }

        Set<String> visited = new HashSet<>();

        Board boardA = doSolve(this, visited, 15, 14);

        Board boardB = doSolve(boardA, visited, 13, 12);

        Board boardC = doSolve(boardB, visited, 11, 10);

        Board boardD = doSolve(boardC, visited, 9, 8);

        Board boardE = doSolve(boardD, visited, 0, 1, 2, 3, 4, 5, 6, 7);

        return boardE;
    }

    private int index(int col, int row) {
        return row * 4 + col;
    }

    private Board doSolve(Board root, Set<String> visited, int... target) {
        PriorityQueue<Board> queue = new PriorityQueue<>(Comparator.comparingInt(a -> a.score));

        root.target(target);
        root.score();
        queue.add(root);

        var board = queue.remove();

        while (board.getManhattanDistance() != 0) {
            List<Board> neighbours = board.neighbours();
            for (Board n : neighbours) {
                var compressed = n.compress();
                if (!visited.contains(compressed)) {
                    queue.add(n);
                    visited.add(compressed);
                }

            }
            board = queue.remove();
        }
        board.block(target);
        return board;
    }

    private void target(int... values) {
        target.clear();
        for (int value : values) {
            target.add(value);
        }
    }

    private void block(int... values) {
        for (int value : values) {
            blocked.add(value);
        }
    }

    private String compress() {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < 16; i++) {
            result.append(board[i] == null ? "n" : board[i]);
            result.append(",");

        }
        return result.toString();
    }

    private void validate() {
        int sum = 0;
        for (int i = 0; i < 16; i++) {
            Integer value = board[i];
            if (value != null) {
                sum++;
            }
        }
        if (sum != 15) {
            throw new RuntimeException("the board is not in a valid state");
        }
    }

    private int manhattanDistance() {
        int sum = 0;
        for (int i = 0; i < 16; i++) {
            if (!target.contains(i)) {
                continue;
            }
            Integer value = board[i];
            int expectedPosition = expectedPosition(value);
            sum += manhattanDistance(i, expectedPosition);
        }
        this.manhattanDistance = sum;
        return sum;

    }

    private int manhattanDistance(int a, int b) {
        if (a == b) {
            return 0;
        }

        int x1 = a / 4;
        int x2 = b / 4;

        int y1 = a % 4;
        int y2 = b % 4;

        return Math.abs(x1 - x2) + Math.abs(y1 - y2);
    }

    private int expectedPosition(Integer value) {
        if (value == null) {
            return 3;
        } else {
            return value;
        }
    }
}
