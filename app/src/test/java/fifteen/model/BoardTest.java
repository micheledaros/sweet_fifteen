package fifteen.model;

import org.junit.jupiter.api.Test;

public class BoardTest {

    @Test
    void scoreOfSolutionIsZero() {
        Board board = new Board();
        board.resetToSolution();
        int score = board.score();
        assert score == 0;
    }

    @Test
    void move() {
        Board board = new Board();
        board.resetToSolution();

        config.Logger.debug(board);

        config.Logger.debug("WEST");
        board.move(Move.W);
        config.Logger.debug(board);

        config.Logger.debug("SOUTH");
        board.move(Move.S);
        config.Logger.debug(board);

        config.Logger.debug("WEST");
        board.move(Move.W);
        config.Logger.debug(board);

        config.Logger.debug("SOUTH");
        board.move(Move.S);
        config.Logger.debug(board);

        config.Logger.debug("EAST");
        board.move(Move.E);
        config.Logger.debug(board);

        config.Logger.debug("EAST");
        board.move(Move.E);
        config.Logger.debug(board);

        config.Logger.debug("NORTH");
        board.move(Move.N);
        config.Logger.debug(board);

        config.Logger.debug("NORTH");
        board.move(Move.N);
        config.Logger.debug(board);
    }

    @Test
    void neighbors1() {
        Board board = new Board();
        board.resetToSolution();
        config.Logger.debug("board");
        config.Logger.debug(board);
        config.Logger.debug("neighbours");

        board.neighbours().stream().forEach(it -> config.Logger.debug(it));

    }

    @Test
    void neighbors2() {
        Board board = new Board();
        board.resetToSolution();
        board.move(Move.W);
        board.move(Move.S);
        config.Logger.debug("board");
        config.Logger.debug(board);
        config.Logger.debug("neighbours");

        board.neighbours().stream().forEach(it -> config.Logger.debug(it));

    }

}
