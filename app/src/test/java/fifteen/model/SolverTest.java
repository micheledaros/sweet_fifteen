package fifteen.model;

import config.Logger;
import org.junit.jupiter.api.Test;

import java.util.List;

public class SolverTest {

    @Test
    void solve0() {
        Board board = new Board();
        board.resetToSolution();
        List<Board> result = board.solve().getSolution();
        config.Logger.debug(result.size());
    }

    @Test
    void solve1() {
        Board board = new Board();
        board.resetToSolution();
        board.move(Move.W);
        List<Board> result = board.solve().getSolution();
        config.Logger.debug(result.size());
    }

    @Test
    void solve2() {
        Board board = new Board();
        board.resetToSolution();
        board.move(Move.W);
        board.move(Move.S);
        List<Board> result = board.solve().getSolution();
        config.Logger.debug(result.size());
    }

    @Test
    void solve6() {
        Board board = new Board();
        board.resetToSolution();
        board.move(Move.W);
        board.move(Move.S);
        board.move(Move.E);
        board.move(Move.E);
        board.move(Move.W);
        board.move(Move.N);

        config.Logger.debug(board);

        Board result = board.solve();
        config.Logger.debug("result:");
        var solution = result.getSolution();
        solution.forEach(Logger::debug);
        config.Logger.debug(solution.size());
    }

    @Test
    void solveComplex() {
        Board board = new Board();

        board.set(0, 6);
        board.set(1, 8);
        board.set(2, 15);
        board.set(3, null);
        board.set(4, 11);
        board.set(5, 0);
        board.set(6, 10);
        board.set(7, 4);
        board.set(8, 12);
        board.set(9, 2);
        board.set(10, 5);
        board.set(11, 1);
        board.set(12, 9);
        board.set(13, 14);
        board.set(14, 13);
        board.set(15, 7);

        Board result = board.solve();
        var solution = result.getSolution();
        config.Logger.debug(solution.size());

    }
    
    
    @Test
    void solveComplex2() {
        Board board = new Board();

        board.set(0, 6);
        board.set(1, 13);
        board.set(2, 0);
        board.set(3, null);
        board.set(4, 1);
        board.set(5, 8);
        board.set(6, 5);
        board.set(7, 2);
        board.set(8, 4);
        board.set(9, 12);
        board.set(10, 7);
        board.set(11, 11);
        board.set(12, 9);
        board.set(13, 14);
        board.set(14, 15);
        board.set(15, 10);

        Board result = board.solve();
        var solution = result.getSolution();
        config.Logger.debug(solution.size());
    }

}

